%global upname Bottleneck

Name:		python3-%{upname}
Version:	1.2.1
Release:	4.3%{?dist}
Summary:	Collection of fast NumPy array functions written in Cython

License:	BSD
URL:		http://berkeleyanalytics.com/bottleneck
Source0:	https://files.pythonhosted.org/packages/source/B/%{upname}/%{upname}-%{version}.tar.gz

%description
%{name} is a collection of fast NumPy array functions
written in Cython.


%package doc
Summary:	Documentation files for %{name}

BuildArch:	noarch

%description doc
This package contains the HTML-docs for %{name}.


%package -n python%{python3_pkgversion}-%{upname}
Summary:	Collection of fast NumPy array functions written in Cython

BuildRequires:	python%{python3_pkgversion}-devel
BuildRequires:	python%{python3_pkgversion}-numpydoc
BuildRequires:	python%{python3_pkgversion}-nose
BuildRequires:	python%{python3_pkgversion}-numpy
BuildRequires:	python%{python3_pkgversion}-scipy
BuildRequires:	python%{python3_pkgversion}-setuptools

Requires:	python%{python3_pkgversion}-numpy%{?_isa}
Requires:	python%{python3_pkgversion}-scipy%{?_isa}

%{?python_provide:%python_provide python%{python3_pkgversion}-%{upname}}

%description -n python%{python3_pkgversion}-%{upname}
python%{python3_pkgversion}-%{upname} is a collection of fast NumPy array functions
written in Cython.

%prep
%autosetup -n %{upname}-%{version} -p 1
%{__rm} -fr .egg* *.egg*


%build
%py3_build


%install
%py3_install

# clean unneeded stuff
%{__rm} -rf %{buildroot}%{python3_sitearch}/bottleneck/src		\
	%{buildroot}%{python3_sitearch}/bottleneck/LICENSE

%{_fixperms} %{buildroot}/*

# Move installed files to temporary location.
%{__mkdir} -p tmp_inst
%{__cp} -fpr %{buildroot}/* tmp_inst

# Build the autodocs.
export PYTHONPATH="$(/bin/pwd)/tmp_inst/%{python3_sitearch}"
%{__mkdir} -p doc/source/_static
#%{_bindir}/sphinx-build -b html doc/source doc/html
unset PYTHONPATH

# Clean unneeded stuff from docs.
%{__rm} -rf doc/html/{.buildinfo,.doctrees}


%check
pushd tmp_inst/%{python3_sitearch}
%{_bindir}/nosetests-%{python3_version} -vv
popd


%files doc
%license bottleneck/LICENSE
%doc README* RELEASE*


%files -n python%{python3_pkgversion}-%{upname}
%license bottleneck/LICENSE
%doc README* RELEASE*
%{python3_sitearch}/bottleneck
%{python3_sitearch}/%{upname}-%{version}-py%{python3_version}.egg-info


%changelog
* Wed Nov 20 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 1.2.1-4.3
- Rename src rpm

* Fri Oct 04 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 1.2.1-4.2
- Drop python3.4, python2 support

* Wed Nov 28 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 1.2.1-4.1
- Add python3.6 support

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Tue May 16 2017 Björn Esser <besser82@fedoraproject.org> - 1.2.1-1
- Updated to new upstream release (rhbz#1451146)

* Sun Apr 09 2017 Björn Esser <besser82@fedoraproject.org> - 1.2.0-1
- Updated to new upstream release (rhbz#1105817)
- Updated spec-file to recent guidelines

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.6.0-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.6.0-10
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-9
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.6.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sun Nov 15 2015 Thomas Spura <tomspur@fedoraproject.org> - 0.6.0-7
- Use new python macros and add python2 subpackage

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-6
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue May 27 2014 Kalev Lember <kalevlember@gmail.com> - 0.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4

* Wed Aug 21 2013 Björn Esser <bjoern.esser@gmail.com> - 0.6.0-1
- Initial rpm release (#999563)
